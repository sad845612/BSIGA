import numpy as np
import csv

cbsd_num = 31 # cbsd numbers
HA = np.zeros(cbsd_num)
DP = np.zeros(cbsd_num)

file_num = 2 # number of files
open_file_name = ['HA','DP']

test_num = 10 # test number
test_num_23 = 100 # test number bigger than 23

# open csv file
for i in range (0,file_num):
    for j in range (5,cbsd_num):
        with open(str(j)+'/'+open_file_name[i]+'.csv') as csvfile:
            rows=csv.reader(csvfile)
            data = []

            # store data
            for row in rows:
                data.append(row)

            if(open_file_name[i]=="DP"):
                DP[j] = data[1][0]
            elif(open_file_name[i]=="HA"):
                HA[j] = data[1][0]
                

for j in range (5,cbsd_num):
    print(DP[j])

with open('check.csv', 'w', newline='') as csvfile:
    writer = csv.writer(csvfile)
    writer.writerow(['time'])
    writer.writerow(['CBSD number','DP','HA','ratio'])
    for j in range (5,cbsd_num):
        if(j<23): writer.writerow([j,int(DP[j])/test_num,int(HA[j])/test_num,(int(HA[j])/test_num)/(int(DP[j])/test_num)])
        else: writer.writerow([j,int(DP[j])/test_num_23,int(HA[j])/test_num,(int(HA[j])/test_num)/(int(DP[j])/test_num_23)])