import numpy as np
import csv

total_num = 49 # folder numbers
total_c = 0 # for count 

DP_t = np.zeros(total_num) # throughput
DP_g = np.zeros(total_num) # generation
DP_r = np.zeros(total_num) # relay number
DP_p = np.zeros((total_num,4)) # power

folder_num = 7
open_folder_f = ['00','05','10','15','20','25','30']
open_folder_b = ['70','75','80','85','90','95','99']

file_num = 1 # number of files
open_file_name = ['GA_mbs_cbsd_diff']

# open csv file
for i in range (0,file_num):
    for j in range (0,folder_num):
        for k in range (0,folder_num):
            with open(open_folder_f[j]+open_folder_b[k]+'/'+open_file_name[i]+'.csv') as csvfile:
                rows=csv.reader(csvfile)
                data = []

                # store data
                for row in rows:
                    data.append(row)

                if(open_file_name[i]=="GA_mbs_cbsd_diff"):
                    DP_t[total_c] = data[3][0]
                    DP_g[total_c] = data[10][0]
                    DP_r[total_c] = data[12][0]
                    for l in range (0,4):
                        DP_p[total_c][l] = data[8][l] 
                    total_c = total_c+1

for j in range (0,total_num):
    print(DP_p[j][3])

total_c = 0
with open('check.csv', 'w', newline='') as csvfile:
    writer = csv.writer(csvfile)
    writer.writerow(['Algorithm result'])
    writer.writerow(['CBSD prob','MBS prob','DP','',''])
    for j in range (0,folder_num):
        for k in range (0,folder_num):
            writer.writerow([open_folder_f[j],open_folder_b[k],DP_t[total_c],DP_g[total_c],DP_r[total_c]])
            total_c = total_c+1

    total_c = 0
    writer.writerow([' '])
    writer.writerow(['BS power'])
    writer.writerow(['CBSD prob','MBS prob','DP','','',''])
    for j in range (0,folder_num):
        for k in range (0,folder_num):
            writer.writerow([open_folder_f[j],open_folder_b[k],DP_p[total_c][0],DP_p[total_c][1],DP_p[total_c][2],float(DP_p[total_c][3])/int(DP_p[total_c][1])])
            total_c = total_c+1
