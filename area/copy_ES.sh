#!/bin/bash

cp 100/2/5/Exhaustive_sthread.c 500/2/5/Exhaustive_sthread.c
cp 100/2/6/Exhaustive_sthread.c 500/2/6/Exhaustive_sthread.c
cp 100/2/7/Exhaustive_sthread.c 500/2/7/Exhaustive_sthread.c
cp 100/2/8/Exhaustive_sthread.c 500/2/8/Exhaustive_sthread.c
cp 100/2/9/Exhaustive_sthread.c 500/2/9/Exhaustive_sthread.c
cp 100/2/10/Exhaustive_sthread.c 500/2/10/Exhaustive_sthread.c

cp 100/3/5/Exhaustive_sthread.c 500/3/5/Exhaustive_sthread.c
cp 100/3/6/Exhaustive_sthread.c 500/3/6/Exhaustive_sthread.c
cp 100/3/7/Exhaustive_sthread.c 500/3/7/Exhaustive_sthread.c
cp 100/3/8/Exhaustive_sthread.c 500/3/8/Exhaustive_sthread.c
cp 100/3/9/Exhaustive_sthread.c 500/3/9/Exhaustive_sthread.c
cp 100/3/10/Exhaustive_sthread.c 500/3/10/Exhaustive_sthread.c

cp 100/4/5/Exhaustive_sthread.c 500/4/5/Exhaustive_sthread.c
cp 100/4/6/Exhaustive_sthread.c 500/4/6/Exhaustive_sthread.c
cp 100/4/7/Exhaustive_sthread.c 500/4/7/Exhaustive_sthread.c
cp 100/4/8/Exhaustive_sthread.c 500/4/8/Exhaustive_sthread.c
cp 100/4/9/Exhaustive_sthread.c 500/4/9/Exhaustive_sthread.c
cp 100/4/10/Exhaustive_sthread.c 500/4/10/Exhaustive_sthread.c

cp 100/5/5/Exhaustive_sthread.c 500/5/5/Exhaustive_sthread.c
cp 100/5/6/Exhaustive_sthread.c 500/5/6/Exhaustive_sthread.c
cp 100/5/7/Exhaustive_sthread.c 500/5/7/Exhaustive_sthread.c
cp 100/5/8/Exhaustive_sthread.c 500/5/8/Exhaustive_sthread.c
cp 100/5/9/Exhaustive_sthread.c 500/5/9/Exhaustive_sthread.c
cp 100/5/10/Exhaustive_sthread.c 500/5/10/Exhaustive_sthread.c

cp 100/6/5/Exhaustive_sthread.c 500/6/5/Exhaustive_sthread.c
cp 100/6/6/Exhaustive_sthread.c 500/6/6/Exhaustive_sthread.c
cp 100/6/7/Exhaustive_sthread.c 500/6/7/Exhaustive_sthread.c
cp 100/6/8/Exhaustive_sthread.c 500/6/8/Exhaustive_sthread.c
cp 100/6/9/Exhaustive_sthread.c 500/6/9/Exhaustive_sthread.c
cp 100/6/10/Exhaustive_sthread.c 500/6/10/Exhaustive_sthread.c

cp 100/7/5/Exhaustive_sthread.c 500/7/5/Exhaustive_sthread.c
cp 100/7/6/Exhaustive_sthread.c 500/7/6/Exhaustive_sthread.c
cp 100/7/7/Exhaustive_sthread.c 500/7/7/Exhaustive_sthread.c
cp 100/7/8/Exhaustive_sthread.c 500/7/8/Exhaustive_sthread.c
cp 100/7/9/Exhaustive_sthread.c 500/7/9/Exhaustive_sthread.c
cp 100/7/10/Exhaustive_sthread.c 500/7/10/Exhaustive_sthread.c

cp 100/8/5/Exhaustive_sthread.c 500/8/5/Exhaustive_sthread.c
cp 100/8/6/Exhaustive_sthread.c 500/8/6/Exhaustive_sthread.c
cp 100/8/7/Exhaustive_sthread.c 500/8/7/Exhaustive_sthread.c
cp 100/8/8/Exhaustive_sthread.c 500/8/8/Exhaustive_sthread.c
cp 100/8/9/Exhaustive_sthread.c 500/8/9/Exhaustive_sthread.c
cp 100/8/10/Exhaustive_sthread.c 500/8/10/Exhaustive_sthread.c

cp 100/2/5/Exhaustive_sthread.c 1000/2/5/Exhaustive_sthread.c
cp 100/2/6/Exhaustive_sthread.c 1000/2/6/Exhaustive_sthread.c
cp 100/2/7/Exhaustive_sthread.c 1000/2/7/Exhaustive_sthread.c
cp 100/2/8/Exhaustive_sthread.c 1000/2/8/Exhaustive_sthread.c
cp 100/2/9/Exhaustive_sthread.c 1000/2/9/Exhaustive_sthread.c
cp 100/2/10/Exhaustive_sthread.c 1000/2/10/Exhaustive_sthread.c

cp 100/3/5/Exhaustive_sthread.c 1000/3/5/Exhaustive_sthread.c
cp 100/3/6/Exhaustive_sthread.c 1000/3/6/Exhaustive_sthread.c
cp 100/3/7/Exhaustive_sthread.c 1000/3/7/Exhaustive_sthread.c
cp 100/3/8/Exhaustive_sthread.c 1000/3/8/Exhaustive_sthread.c
cp 100/3/9/Exhaustive_sthread.c 1000/3/9/Exhaustive_sthread.c
cp 100/3/10/Exhaustive_sthread.c 1000/3/10/Exhaustive_sthread.c

cp 100/4/5/Exhaustive_sthread.c 1000/4/5/Exhaustive_sthread.c
cp 100/4/6/Exhaustive_sthread.c 1000/4/6/Exhaustive_sthread.c
cp 100/4/7/Exhaustive_sthread.c 1000/4/7/Exhaustive_sthread.c
cp 100/4/8/Exhaustive_sthread.c 1000/4/8/Exhaustive_sthread.c
cp 100/4/9/Exhaustive_sthread.c 1000/4/9/Exhaustive_sthread.c
cp 100/4/10/Exhaustive_sthread.c 1000/4/10/Exhaustive_sthread.c

cp 100/5/5/Exhaustive_sthread.c 1000/5/5/Exhaustive_sthread.c
cp 100/5/6/Exhaustive_sthread.c 1000/5/6/Exhaustive_sthread.c
cp 100/5/7/Exhaustive_sthread.c 1000/5/7/Exhaustive_sthread.c
cp 100/5/8/Exhaustive_sthread.c 1000/5/8/Exhaustive_sthread.c
cp 100/5/9/Exhaustive_sthread.c 1000/5/9/Exhaustive_sthread.c
cp 100/5/10/Exhaustive_sthread.c 1000/5/10/Exhaustive_sthread.c

cp 100/6/5/Exhaustive_sthread.c 1000/6/5/Exhaustive_sthread.c
cp 100/6/6/Exhaustive_sthread.c 1000/6/6/Exhaustive_sthread.c
cp 100/6/7/Exhaustive_sthread.c 1000/6/7/Exhaustive_sthread.c
cp 100/6/8/Exhaustive_sthread.c 1000/6/8/Exhaustive_sthread.c
cp 100/6/9/Exhaustive_sthread.c 1000/6/9/Exhaustive_sthread.c
cp 100/6/10/Exhaustive_sthread.c 1000/6/10/Exhaustive_sthread.c

cp 100/7/5/Exhaustive_sthread.c 1000/7/5/Exhaustive_sthread.c
cp 100/7/6/Exhaustive_sthread.c 1000/7/6/Exhaustive_sthread.c
cp 100/7/7/Exhaustive_sthread.c 1000/7/7/Exhaustive_sthread.c
cp 100/7/8/Exhaustive_sthread.c 1000/7/8/Exhaustive_sthread.c
cp 100/7/9/Exhaustive_sthread.c 1000/7/9/Exhaustive_sthread.c
cp 100/7/10/Exhaustive_sthread.c 1000/7/10/Exhaustive_sthread.c

cp 100/8/5/Exhaustive_sthread.c 1000/8/5/Exhaustive_sthread.c
cp 100/8/6/Exhaustive_sthread.c 1000/8/6/Exhaustive_sthread.c
cp 100/8/7/Exhaustive_sthread.c 1000/8/7/Exhaustive_sthread.c
cp 100/8/8/Exhaustive_sthread.c 1000/8/8/Exhaustive_sthread.c
cp 100/8/9/Exhaustive_sthread.c 1000/8/9/Exhaustive_sthread.c
cp 100/8/10/Exhaustive_sthread.c 1000/8/10/Exhaustive_sthread.c

echo  "\n\nFinished! \a \n"
