#!/bin/bash

cd 100
sh throughput_uniform_DP.sh
cd ..
cd 500
sh throughput_uniform_DP.sh
cd ..
cd 1000
sh throughput_uniform_DP.sh
cd ..

echo  "\n\nFinished! \a \n"
