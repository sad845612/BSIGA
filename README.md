# BSIGA

## A. Environmental Setting
* HW Configuration: 
    * CPU : Intel(R) Core(TM) i7-9700K CPU @ 3.60GHz
    * RAM : 32G
* SW Configuration
    * kernel : Linux 5.4.0-70-generic
    * gcc version : 7.5.0
    * python3 version : 3.6.9

## B. File introduction

https://hackmd.io/@LfeEBoZHQDKwDtie8uoeeA/r1Vy1JoHd
