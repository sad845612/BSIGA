import matplotlib.pyplot as plt
import numpy as np
import csv

plt.axis([-2050,2050,-2050,2050]) # limit whole environment
# plt.xlabel('x axis distance (m)', fontsize=16)
# plt.ylabel('y axis distance (m)', fontsize=16)

cbsd_num = 30 # cbsd numbers
building_num = 30*2 # building number (record the left and right coordinate)
ue_num = 1200 # ue numbers

cbsd = [[0] * 2 for i in range(cbsd_num)] # declare cbsd two-dimensional array
building = [[0] * 2 for i in range(building_num)] # declare building two-dimensional array
ue = [[0] * 3 for i in range(ue_num)] # declare ue two-dimensional array

# open csv file
with open('node_nonuniform_1200.csv') as csvfile:
    rows=csv.reader(csvfile)
    data = []

    # store data
    for row in rows:
        print(row)
        data.append(row)

    print("\n---------\n\nrecord\n")

    # mbs coordinate
    mbs_x = int(data[1][0])
    mbs_y = int(data[1][1])
    print(mbs_x, mbs_y)

    # cbsd coordinate
    for i in range(0, cbsd_num):
        cbsd[i][0] = int(data[4+i][0])
        cbsd[i][1] = int(data[4+i][1])
        print(cbsd[i][0], cbsd[i][1])
    
    # ue parameters
    area1=int(data[8 + cbsd_num + building_num][0])
    area2=int(data[8 + cbsd_num + building_num][1])
    area3=int(data[8 + cbsd_num + building_num][2])
    area4=int(data[8 + cbsd_num + building_num][3])
    print(area1, area2, area3, area4)

    for i in range(0, area1):
        ue[i][0] = int(data[12 + cbsd_num + building_num + i][0])
        ue[i][1] = int(data[12 + cbsd_num + building_num + i][1])
        plt.plot(ue[i][0], ue[i][1], '.', color='#008B8B')
        ue[i][2] = int(data[12 + cbsd_num + building_num + i][2])
#        if ue[i][2] == 1 :
#            plt.plot(ue[i][0], ue[i][1], '.', color='r')
        print(ue[i][0], ue[i][1])
    plt.plot(ue[0][0], ue[0][1], '.', label='UE', color='#008B8B')
    for i in range(0, area2):
        ue[i][0] = int(data[13 + area1 + cbsd_num + building_num + i][0])
        ue[i][1] = int(data[13 + area1 + cbsd_num + building_num + i][1])
        plt.plot(ue[i][0], ue[i][1], '.', color='#008B8B')
        ue[i][2] = int(data[13 + area1 + cbsd_num + building_num + i][2])
#        if ue[i][2] == 1 :
#            plt.plot(ue[i][0], ue[i][1], '.', color='r')
        print(ue[i][0], ue[i][1])
    for i in range(0, area3):
        ue[i][0] = int(data[14 + area1 + area2 + cbsd_num + building_num + i][0])
        ue[i][1] = int(data[14 + area1 + area2 + cbsd_num + building_num + i][1])
        plt.plot(ue[i][0], ue[i][1], '.', color='#008B8B')
        ue[i][2] = int(data[14 + area1 + area2 + cbsd_num + building_num + i][2])
#        if ue[i][2] == 1 :
#            plt.plot(ue[i][0], ue[i][1], '.', color='r')
        print(ue[i][0], ue[i][1])
    for i in range(0, area4):
        ue[i][0] = int(data[15 + area1 + area2 + area3 + cbsd_num + building_num + i][0])
        ue[i][1] = int(data[15 + area1 + area2 + area3 + cbsd_num + building_num + i][1])
        plt.plot(ue[i][0], ue[i][1], '.', color='#008B8B')
        ue[i][2] = int(data[15 + area1 + area2 + area3 + cbsd_num + building_num + i][2])
#        if ue[i][2] == 1 :
#            plt.plot(ue[i][0], ue[i][1], '.', color='r')
        print(ue[i][0], ue[i][1])

    # draw mbs
    plt.plot(mbs_x, mbs_y, 's', label='MBS', color='#DAA520', markersize=10)
    
    # draw cbsd
    plt.plot(cbsd[0][0], cbsd[0][1], '*', label='RN', color='#A52A2A', markersize=5)
    for i in range(1, cbsd_num):
        plt.plot(cbsd[i][0], cbsd[i][1], '*', color='#A52A2A', markersize=5)
        #plt.plot([cbsd[i][0], mbs_x], [cbsd[i][1], mbs_y], ':', color='#8A2BE2')

    
'''
    # building coordinate
    for i in range(0, building_num):
        building[i][0] = int(data[6 + cbsd_num + i][0])
        building[i][1] = int(data[6 + cbsd_num + i][1])
        print(building[i][0], building[i][1])
    # draw building
    plt.plot([building[0][0], building[1][0]], [building[0][1], building[1][1]], 'k', label='building')
    for i in range(1, int(building_num/2)):
        plt.plot([building[i*2][0], building[i*2+1][0]], [building[i*2][1], building[i*2+1][1]], 'k')
'''
#plt.plot(0, 0, ':', label='cbsd connect', color='#FFD700')
#plt.plot(0, 0, '--', label='UE connect')

plt.legend(loc='upper right',framealpha=0.5)
#plt.legend(loc='lower center',ncol=4,framealpha=0.5)
plt.grid()
plt.show()
