import numpy as np
import csv

cbsd_num = 31 # cbsd numbers

DP_single_t = np.zeros(cbsd_num) # throughput
DP_single_g = np.zeros(cbsd_num) # generation
DP_single_r = np.zeros(cbsd_num) # relay number
DP_single_p = np.zeros((cbsd_num,4)) # power

DP_t = np.zeros(cbsd_num) # throughput
DP_g = np.zeros(cbsd_num) # generation
DP_r = np.zeros(cbsd_num) # relay number
DP_p = np.zeros((cbsd_num,4)) # power

file_num = 2 # number of files
open_file_name = ['GA_mbs_cbsd_diff','GA_mbs_cbsd_diff_single']

# open csv file
for i in range (0,file_num):
    for j in range (5,cbsd_num):
        with open(str(j)+'/'+open_file_name[i]+'.csv') as csvfile:
            rows=csv.reader(csvfile)
            data = []

            # store data
            for row in rows:
                data.append(row)

            if(open_file_name[i]=="GA_mbs_cbsd_diff"):
                DP_t[j] = data[3][0]
                DP_g[j] = data[10][0]
                DP_r[j] = data[12][0]
                for k in range (0,4):
                    DP_p[j][k] = data[8][k]
            elif(open_file_name[i]=="GA_mbs_cbsd_diff_single"):
                DP_single_t[j] = data[3][0]
                DP_single_g[j] = data[10][0]
                DP_single_r[j] = data[12][0]
                for k in range (0,4):
                    DP_single_p[j][k] = data[8][k]

for j in range (5,cbsd_num):
    print(DP_single_t[j])

with open('check.csv', 'w', newline='') as csvfile:
    writer = csv.writer(csvfile)
    writer.writerow(['Algorithm result'])
    writer.writerow(['CBSD number','DP','','','DP single crossover','',''])
    for j in range (5,cbsd_num):
        writer.writerow([j,DP_t[j],DP_g[j],DP_r[j],DP_single_t[j],DP_single_g[j],DP_single_r[j]])
    
    writer.writerow([''])
    writer.writerow(['BS power'])
    writer.writerow(['CBSD number','DP','','','','DP single crossover','','',''])
    for j in range (5,cbsd_num):
        writer.writerow([j,DP_p[j][0],DP_p[j][1],DP_p[j][2],float(DP_p[j][3])/int(DP_p[j][1]),DP_single_p[j][0],DP_single_p[j][1],DP_single_p[j][2],float(DP_single_p[j][3])/int(DP_single_p[j][1])])
