import numpy as np
import csv

cbsd_num = 31 # cbsd numbers
HA_t = np.zeros(cbsd_num) # throughput
HA_g = np.zeros(cbsd_num) # generation
HA_r = np.zeros(cbsd_num) # relay number
HA_p = np.zeros((cbsd_num,4)) # power

DP_t = np.zeros(cbsd_num) # throughput
DP_g = np.zeros(cbsd_num) # generation
DP_r = np.zeros(cbsd_num) # relay number
DP_p = np.zeros((cbsd_num,4)) # power

BSIGA_t = np.zeros(cbsd_num) # throughput
BSIGA_g = np.zeros(cbsd_num) # generation
BSIGA_r = np.zeros(cbsd_num) # relay number
BSIGA_p = np.zeros((cbsd_num,4)) # power

modified_t = np.zeros(cbsd_num) # throughput
modified_g = np.zeros(cbsd_num) # generation
modified_r = np.zeros(cbsd_num) # relay number
modified_p = np.zeros((cbsd_num,4)) # power

BARSPAA_t = np.zeros(cbsd_num) # throughput
BARSPAA_r = np.zeros(cbsd_num) # relay number
BARSPAA_p = np.zeros((cbsd_num,4)) # power

ES_t = np.zeros(cbsd_num) # throughput

file_num = 5 # number of files
open_file_name = ['non_modified_GA','non_DP','non_HA','non_wf_3600','non_ES']

# open csv file
for i in range (0,file_num):
    for j in range (5,cbsd_num):
        with open(str(j)+'/'+open_file_name[i]+'.csv') as csvfile:
            rows=csv.reader(csvfile)
            data = []

            # store data
            for row in rows:
                data.append(row)

            if(open_file_name[i]=="non_modified_GA"):
                modified_t[j] = data[3][0]
                modified_g[j] = data[10][0]
                modified_r[j] = data[12][0]
                for k in range (0,4):
                    modified_p[j][k] = data[8][k]
            elif(open_file_name[i]=="non_DP"):
                DP_t[j] = data[3][0]
                DP_g[j] = data[10][0]
                DP_r[j] = data[12][0]
                for k in range (0,4):
                    DP_p[j][k] = data[8][k]
            elif(open_file_name[i]=="non_HA"):
                HA_t[j] = data[3][0]
                HA_g[j] = data[10][0]
                HA_r[j] = data[12][0]
                for k in range (0,4):
                    HA_p[j][k] = data[8][k]
            elif(open_file_name[i]=="non_wf_3600"):
                BARSPAA_t[j] = data[3][0]
                BARSPAA_r[j] = data[10][0]
                for k in range (0,4):
                    BARSPAA_p[j][k] = data[8][k]
            elif(open_file_name[i]=="non_ES"):
                ES_t[j] = data[6][0]

for j in range (5,cbsd_num):
    print(modified_t[j])

with open('check.csv', 'w', newline='') as csvfile:
    writer = csv.writer(csvfile)
    writer.writerow(['Algorithm result'])
    writer.writerow(['CBSD number','Modified GA','','','DP','','','HA','','','BARSPAA','','ES'])
    for j in range (5,cbsd_num):
        writer.writerow([j,modified_t[j],modified_g[j],modified_r[j],DP_t[j],DP_g[j],DP_r[j],HA_t[j],HA_g[j],HA_r[j],BARSPAA_t[j],BARSPAA_r[j],ES_t[j]])
    
    writer.writerow([''])
    writer.writerow(['BS power'])
    writer.writerow(['CBSD number','Modified GA','','','','DP','','','','HA','','','','BARSPAA','','',''])
    for j in range (5,cbsd_num):
        writer.writerow([j,modified_p[j][0],modified_p[j][1],modified_p[j][2],float(modified_p[j][3])/int(modified_p[j][1]),DP_p[j][0],DP_p[j][1],DP_p[j][2],float(DP_p[j][3])/int(DP_p[j][1]),HA_p[j][0],HA_p[j][1],HA_p[j][2],float(HA_p[j][3])/int(HA_p[j][1]),BARSPAA_p[j][0],BARSPAA_p[j][1],BARSPAA_p[j][2],float(BARSPAA_p[j][3])/int(BARSPAA_p[j][1])])
