#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <string.h>

void read_BARSPAA(int folder_num,int ue_num,int test_num,float* WF_t,int* WF_r){
    int MAX_LINE_SIZE=110;
	char line[MAX_LINE_SIZE];
	char *result = NULL;
	int row_c=0,col_c=0,i=5;
	float data[ue_num+30][test_num]; // csv data rows , cbsd_num columns
	FILE *fpr=NULL;

	switch(folder_num){
		case 5:
			fpr=fopen("5/wf_2600.csv","r");
			//record the data to array
			while(fgets(line, MAX_LINE_SIZE, fpr) != NULL) {
				result=strtok(line, ",");
				while(result != NULL) {
					data[row_c][col_c]=atof(result);
					col_c++;
					result=strtok(NULL, ",");
				}
				col_c=0;
				row_c++;
			}
			WF_t[folder_num]=data[4][0];
			WF_r[folder_num]=data[6][0];

			break;
	}

	fclose(fpr);
}

void read_HA(int folder_num,int ue_num,int test_num,float* HA_t,float* HA_g,int* HA_r){
    int MAX_LINE_SIZE=110;
	char line[MAX_LINE_SIZE];
	char *result = NULL;
	int row_c=0,col_c=0,i=5;
	float data[ue_num+30][test_num]; // csv data rows , cbsd_num columns
	FILE *fpr=NULL;

	switch(folder_num){
		case 5:
			fpr=fopen("5/GA_mbs_base.csv","r");
			//record the data to array
			while(fgets(line, MAX_LINE_SIZE, fpr) != NULL) {
				result=strtok(line, ",");
				while(result != NULL) {
					data[row_c][col_c]=atof(result);
					col_c++;
					result=strtok(NULL, ",");
				}
				col_c=0;
				row_c++;
			}
			HA_t[folder_num]=data[4][0];
			HA_g[folder_num]=data[6][0];
			HA_r[folder_num]=data[8][0];

			break;
	}

	fclose(fpr);
}

void read_DP(int folder_num,int ue_num,int test_num,float* DP_t,float* DP_g,int* DP_r){
    int MAX_LINE_SIZE=110;
	char line[MAX_LINE_SIZE];
	char *result = NULL;
	int row_c=0,col_c=0,i=5;
	float data[ue_num+30][test_num]; // csv data rows , cbsd_num columns
	FILE *fpr=NULL;

	switch(folder_num){
		case 5:
			fpr=fopen("5/GA_mbs_cbsd_diff.csv","r");
			//record the data to array
			while(fgets(line, MAX_LINE_SIZE, fpr) != NULL) {
				result=strtok(line, ",");
				while(result != NULL) {
					data[row_c][col_c]=atof(result);
					col_c++;
					result=strtok(NULL, ",");
				}
				col_c=0;
				row_c++;
			}
			DP_t[folder_num]=data[4][0];
			DP_g[folder_num]=data[6][0];
			DP_r[folder_num]=data[8][0];

			break;
	}

	fclose(fpr);
}

void read_modified_GA(int folder_num,int ue_num,int test_num,float* modified_t,float* modified_g,int* modified_r){
    int MAX_LINE_SIZE=110;
	char line[MAX_LINE_SIZE];
	char *result = NULL;
	int row_c=0,col_c=0,i=5;
	float data[ue_num+30][test_num]; // csv data rows , cbsd_num columns
	FILE *fpr=NULL;

	switch(folder_num){
		case 5:
			fpr=fopen("5/modified_GA.csv","r");
			//record the data to array
			while(fgets(line, MAX_LINE_SIZE, fpr) != NULL) {
				result=strtok(line, ",");
				while(result != NULL) {
					data[row_c][col_c]=atof(result);
					col_c++;
					result=strtok(NULL, ",");
				}
				col_c=0;
				row_c++;
			}
			// for(row_c=0;row_c<10;row_c++) printf("%f\n",data[row_c][0]);
			modified_t[folder_num]=data[4][0];
			modified_g[folder_num]=data[6][0];
			modified_r[folder_num]=data[8][0];

			break;
	}

	fclose(fpr);
}

void read_ES(int folder_num,int ue_num,int test_num,float* ES_t){
    int MAX_LINE_SIZE=110;
	char line[MAX_LINE_SIZE];
	char *result = NULL;
	int row_c=0,col_c=0,i=5;
	float data[ue_num+30][test_num]; // csv data rows , cbsd_num columns
	FILE *fpr=NULL;

	switch(folder_num){
		case 5:
			fpr=fopen("5/Exhaustive.csv","r");
			//record the data to array
			while(fgets(line, MAX_LINE_SIZE, fpr) != NULL) {
				result=strtok(line, ",");
				while(result != NULL) {
					data[row_c][col_c]=atof(result);
					col_c++;
					result=strtok(NULL, ",");
				}
				col_c=0;
				row_c++;
			}
			//for(row_c=0;row_c<10;row_c++) printf("%f\n",data[row_c][0]);
			//printf("\n");
			ES_t[folder_num]=data[6][0];
			break;
		case 6:
			fpr=fopen("6/Exhaustive.csv","r");
			//record the data to array
			while(fgets(line, MAX_LINE_SIZE, fpr) != NULL) {
				result=strtok(line, ",");
				while(result != NULL) {
					data[row_c][col_c]=atof(result);
					col_c++;
					result=strtok(NULL, ",");
				}
				col_c=0;
				row_c++;
			}
			ES_t[folder_num]=data[6][0];
			break;
		case 7:
			fpr=fopen("7/Exhaustive.csv","r");
			//record the data to array
			while(fgets(line, MAX_LINE_SIZE, fpr) != NULL) {
				result=strtok(line, ",");
				while(result != NULL) {
					data[row_c][col_c]=atof(result);
					col_c++;
					result=strtok(NULL, ",");
				}
				col_c=0;
				row_c++;
			}
			ES_t[folder_num]=data[6][0];
			break;
		case 8:
			fpr=fopen("8/Exhaustive.csv","r");
			//record the data to array
			while(fgets(line, MAX_LINE_SIZE, fpr) != NULL) {
				result=strtok(line, ",");
				while(result != NULL) {
					data[row_c][col_c]=atof(result);
					col_c++;
					result=strtok(NULL, ",");
				}
				col_c=0;
				row_c++;
			}
			ES_t[folder_num]=data[6][0];
			break;
		case 9:
			fpr=fopen("9/Exhaustive.csv","r");
			//record the data to array
			while(fgets(line, MAX_LINE_SIZE, fpr) != NULL) {
				result=strtok(line, ",");
				while(result != NULL) {
					data[row_c][col_c]=atof(result);
					col_c++;
					result=strtok(NULL, ",");
				}
				col_c=0;
				row_c++;
			}
			ES_t[folder_num]=data[6][0];
			break;
		case 10:
			fpr=fopen("10/Exhaustive.csv","r");
			//record the data to array
			while(fgets(line, MAX_LINE_SIZE, fpr) != NULL) {
				result=strtok(line, ",");
				while(result != NULL) {
					data[row_c][col_c]=atof(result);
					col_c++;
					result=strtok(NULL, ",");
				}
				col_c=0;
				row_c++;
			}
			for(row_c=0;row_c<10;row_c++) printf("%f\n",data[row_c][0]);
			printf("\n");
			ES_t[folder_num]=data[6][0];
			break;
		case 11:
			fpr=fopen("11/Exhaustive.csv","r");
			//record the data to array
			while(fgets(line, MAX_LINE_SIZE, fpr) != NULL) {
				result=strtok(line, ",");
				while(result != NULL) {
					data[row_c][col_c]=atof(result);
					col_c++;
					result=strtok(NULL, ",");
				}
				col_c=0;
				row_c++;
			}
			ES_t[folder_num]=data[6][0];
			break;
		case 12:
			fpr=fopen("12/Exhaustive.csv","r");
			//record the data to array
			while(fgets(line, MAX_LINE_SIZE, fpr) != NULL) {
				result=strtok(line, ",");
				while(result != NULL) {
					data[row_c][col_c]=atof(result);
					col_c++;
					result=strtok(NULL, ",");
				}
				col_c=0;
				row_c++;
			}
			ES_t[folder_num]=data[6][0];
			break;
		case 13:
			fpr=fopen("13/Exhaustive.csv","r");
			//record the data to array
			while(fgets(line, MAX_LINE_SIZE, fpr) != NULL) {
				result=strtok(line, ",");
				while(result != NULL) {
					data[row_c][col_c]=atof(result);
					col_c++;
					result=strtok(NULL, ",");
				}
				col_c=0;
				row_c++;
			}
			ES_t[folder_num]=data[6][0];
			break;
		case 14:
			fpr=fopen("14/Exhaustive.csv","r");
			//record the data to array
			while(fgets(line, MAX_LINE_SIZE, fpr) != NULL) {
				result=strtok(line, ",");
				while(result != NULL) {
					data[row_c][col_c]=atof(result);
					col_c++;
					result=strtok(NULL, ",");
				}
				col_c=0;
				row_c++;
			}
			ES_t[folder_num]=data[6][0];
			break;
		case 15:
			fpr=fopen("15/Exhaustive.csv","r");
			//record the data to array
			while(fgets(line, MAX_LINE_SIZE, fpr) != NULL) {
				result=strtok(line, ",");
				while(result != NULL) {
					data[row_c][col_c]=atof(result);
					col_c++;
					result=strtok(NULL, ",");
				}
				col_c=0;
				row_c++;
			}
			ES_t[folder_num]=data[6][0];
			break;
		case 16:
			fpr=fopen("16/Exhaustive.csv","r");
			//record the data to array
			while(fgets(line, MAX_LINE_SIZE, fpr) != NULL) {
				result=strtok(line, ",");
				while(result != NULL) {
					data[row_c][col_c]=atof(result);
					col_c++;
					result=strtok(NULL, ",");
				}
				col_c=0;
				row_c++;
			}
			ES_t[folder_num]=data[6][0];
			break;
		case 17:
			fpr=fopen("17/Exhaustive.csv","r");
			//record the data to array
			while(fgets(line, MAX_LINE_SIZE, fpr) != NULL) {
				result=strtok(line, ",");
				while(result != NULL) {
					data[row_c][col_c]=atof(result);
					col_c++;
					result=strtok(NULL, ",");
				}
				col_c=0;
				row_c++;
			}
			ES_t[folder_num]=data[6][0];
			break;
		case 18:
			fpr=fopen("18/Exhaustive.csv","r");
			//record the data to array
			while(fgets(line, MAX_LINE_SIZE, fpr) != NULL) {
				result=strtok(line, ",");
				while(result != NULL) {
					data[row_c][col_c]=atof(result);
					col_c++;
					result=strtok(NULL, ",");
				}
				col_c=0;
				row_c++;
			}
			ES_t[folder_num]=data[6][0];
			break;
		case 19:
			fpr=fopen("19/Exhaustive.csv","r");
			//record the data to array
			while(fgets(line, MAX_LINE_SIZE, fpr) != NULL) {
				result=strtok(line, ",");
				while(result != NULL) {
					data[row_c][col_c]=atof(result);
					col_c++;
					result=strtok(NULL, ",");
				}
				col_c=0;
				row_c++;
			}
			ES_t[folder_num]=data[6][0];
			break;
		case 20:
			fpr=fopen("20/Exhaustive.csv","r");
			//record the data to array
			while(fgets(line, MAX_LINE_SIZE, fpr) != NULL) {
				result=strtok(line, ",");
				while(result != NULL) {
					data[row_c][col_c]=atof(result);
					col_c++;
					result=strtok(NULL, ",");
				}
				col_c=0;
				row_c++;
			}
			ES_t[folder_num]=data[6][0];
			break;
		case 21:
			fpr=fopen("21/Exhaustive.csv","r");
			//record the data to array
			while(fgets(line, MAX_LINE_SIZE, fpr) != NULL) {
				result=strtok(line, ",");
				while(result != NULL) {
					data[row_c][col_c]=atof(result);
					col_c++;
					result=strtok(NULL, ",");
				}
				col_c=0;
				row_c++;
			}
			ES_t[folder_num]=data[6][0];
			break;
		case 22:
			fpr=fopen("22/Exhaustive.csv","r");
			//record the data to array
			while(fgets(line, MAX_LINE_SIZE, fpr) != NULL) {
				result=strtok(line, ",");
				while(result != NULL) {
					data[row_c][col_c]=atof(result);
					col_c++;
					result=strtok(NULL, ",");
				}
				col_c=0;
				row_c++;
			}
			ES_t[folder_num]=data[6][0];
			break;
		case 23:
			fpr=fopen("23/Exhaustive.csv","r");
			//record the data to array
			while(fgets(line, MAX_LINE_SIZE, fpr) != NULL) {
				result=strtok(line, ",");
				while(result != NULL) {
					data[row_c][col_c]=atof(result);
					col_c++;
					result=strtok(NULL, ",");
				}
				col_c=0;
				row_c++;
			}
			ES_t[folder_num]=data[6][0];
			break;
		case 24:
			fpr=fopen("24/Exhaustive.csv","r");
			//record the data to array
			while(fgets(line, MAX_LINE_SIZE, fpr) != NULL) {
				result=strtok(line, ",");
				while(result != NULL) {
					data[row_c][col_c]=atof(result);
					col_c++;
					result=strtok(NULL, ",");
				}
				col_c=0;
				row_c++;
			}
			ES_t[folder_num]=data[6][0];
			break;
		case 25:
			fpr=fopen("25/Exhaustive.csv","r");
			//record the data to array
			while(fgets(line, MAX_LINE_SIZE, fpr) != NULL) {
				result=strtok(line, ",");
				while(result != NULL) {
					data[row_c][col_c]=atof(result);
					col_c++;
					result=strtok(NULL, ",");
				}
				col_c=0;
				row_c++;
			}
			ES_t[folder_num]=data[6][0];
			break;
		case 26:
			fpr=fopen("26/Exhaustive.csv","r");
			//record the data to array
			while(fgets(line, MAX_LINE_SIZE, fpr) != NULL) {
				result=strtok(line, ",");
				while(result != NULL) {
					data[row_c][col_c]=atof(result);
					col_c++;
					result=strtok(NULL, ",");
				}
				col_c=0;
				row_c++;
			}
			ES_t[folder_num]=data[6][0];
			break;
		case 27:
			fpr=fopen("27/Exhaustive.csv","r");
			//record the data to array
			while(fgets(line, MAX_LINE_SIZE, fpr) != NULL) {
				result=strtok(line, ",");
				while(result != NULL) {
					data[row_c][col_c]=atof(result);
					col_c++;
					result=strtok(NULL, ",");
				}
				col_c=0;
				row_c++;
			}
			ES_t[folder_num]=data[6][0];
			break;
		case 28:
			fpr=fopen("28/Exhaustive.csv","r");
			//record the data to array
			while(fgets(line, MAX_LINE_SIZE, fpr) != NULL) {
				result=strtok(line, ",");
				while(result != NULL) {
					data[row_c][col_c]=atof(result);
					col_c++;
					result=strtok(NULL, ",");
				}
				col_c=0;
				row_c++;
			}
			ES_t[folder_num]=data[6][0];
			break;
		case 29:
			fpr=fopen("29/Exhaustive.csv","r");
			//record the data to array
			while(fgets(line, MAX_LINE_SIZE, fpr) != NULL) {
				result=strtok(line, ",");
				while(result != NULL) {
					data[row_c][col_c]=atof(result);
					col_c++;
					result=strtok(NULL, ",");
				}
				col_c=0;
				row_c++;
			}
			ES_t[folder_num]=data[6][0];
			break;
		case 30:
			fpr=fopen("30/Exhaustive.csv","r");
			//record the data to array
			while(fgets(line, MAX_LINE_SIZE, fpr) != NULL) {
				result=strtok(line, ",");
				while(result != NULL) {
					data[row_c][col_c]=atof(result);
					col_c++;
					result=strtok(NULL, ",");
				}
				col_c=0;
				row_c++;
			}
			ES_t[folder_num]=data[6][0];
			break;
	}

	fclose(fpr);
}

int main(){
	int cbsd_num=31;
	int ue_num=2600;
	int test_num=100;
    float modified_t[cbsd_num],modified_g[cbsd_num],DP_t[cbsd_num],DP_g[cbsd_num],HA_t[cbsd_num],HA_g[cbsd_num],WF_t[cbsd_num],ES_t[cbsd_num];
    int modified_r[cbsd_num],DP_r[cbsd_num],HA_r[cbsd_num],WF_r[cbsd_num];
	int i=5;

    while(i<cbsd_num){
		read_ES(i,ue_num,test_num,&ES_t[0]); // read ES	
		//read_modified_GA(i,ue_num,test_num,&modified_t[0],&modified_g[0],&modified_r[0]); // read modified GA			
		//read_DP(i,ue_num,test_num,&DP_t[0],&DP_g[0],&DP_r[0]); // read DP
		//read_HA(i,ue_num,test_num,&HA_t[0],&HA_g[0],&HA_r[0]); // read HA
		//read_BARSPAA(i,ue_num,test_num,&WF_t[0],&WF_r[0]); // read BARSPAA
		
		i++;
    }
	
	for(i=5;i<cbsd_num;i++) printf("%f ",ES_t[i]);
	printf("\nES : %f, Modified GA : %f, DP : %f, HA : %f, WF : %f\n",ES_t[5],modified_t[5],DP_t[5],HA_t[5],WF_t[5]);
	printf("hello world\n");

	return 0;
}