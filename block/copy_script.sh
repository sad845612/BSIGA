#!/bin/bash

cp 0/throughput_uniform_DP_1200.sh 5/
cp 0/throughput_uniform_HA_1200.sh 5/
cp 0/throughput_uniform_modified\ GA_1200.sh 5/

cp 0/throughput_uniform_DP_1200.sh 10/
cp 0/throughput_uniform_HA_1200.sh 10/
cp 0/throughput_uniform_modified\ GA_1200.sh 10/

cp 0/throughput_uniform_DP_1200.sh 15/
cp 0/throughput_uniform_HA_1200.sh 15/
cp 0/throughput_uniform_modified\ GA_1200.sh 15/

cp 0/throughput_uniform_DP_1200.sh 20/
cp 0/throughput_uniform_HA_1200.sh 20/
cp 0/throughput_uniform_modified\ GA_1200.sh 20/

cp 0/throughput_uniform_DP_1200.sh 25/
cp 0/throughput_uniform_HA_1200.sh 25/
cp 0/throughput_uniform_modified\ GA_1200.sh 25/

cp 0/throughput_uniform_DP_1200.sh 30/
cp 0/throughput_uniform_HA_1200.sh 30/
cp 0/throughput_uniform_modified\ GA_1200.sh 30/

cp 0/throughput_uniform_DP_1200.sh 35/
cp 0/throughput_uniform_HA_1200.sh 35/
cp 0/throughput_uniform_modified\ GA_1200.sh 35/

cp 0/throughput_uniform_DP_1200.sh 40/
cp 0/throughput_uniform_HA_1200.sh 40/
cp 0/throughput_uniform_modified\ GA_1200.sh 40/

cp 0/throughput_uniform_DP_1200.sh 45/
cp 0/throughput_uniform_HA_1200.sh 45/
cp 0/throughput_uniform_modified\ GA_1200.sh 45/

cp 0/throughput_uniform_DP_1200.sh 50/
cp 0/throughput_uniform_HA_1200.sh 50/
cp 0/throughput_uniform_modified\ GA_1200.sh 50/

echo  "\n\nFinished! \a \n"
