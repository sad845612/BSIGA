#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <string.h>
#include <pthread.h>

int main(){
	float mbs_TX_power[5]={5,6.25,7.5,8.75,10}; // unit : w
	float mbs_cbsd_TX_power[2]={7.5,0}; // unit : w
	float m_a_p[4],m_c_p[30]; // mbs area 1~4 and mbs to cbsd power
	int m_c[4],m_c_c[30]; // for count
	float sum_p=0.0,sum_h=0.0;
	int all_c=0,good_c=0,high_c=0,better_c=0;
	int i;

	FILE *fpw=NULL;
	fpw=fopen("good_allocation.csv","w");

	for(m_c[0]=0;m_c[0]<5;m_c[0]++){
		m_a_p[0]=mbs_TX_power[m_c[0]];
		for(m_c[1]=0;m_c[1]<5;m_c[1]++){
			m_a_p[1]=mbs_TX_power[m_c[1]];
			for(m_c[2]=0;m_c[2]<5;m_c[2]++){
				m_a_p[2]=mbs_TX_power[m_c[2]];
				for(m_c[3]=0;m_c[3]<5;m_c[3]++){
					m_a_p[3]=mbs_TX_power[m_c[3]];
					for(m_c_c[0]=0;m_c_c[0]<2;m_c_c[0]++){
						m_c_p[0]=mbs_cbsd_TX_power[m_c_c[0]];
						for(m_c_c[1]=0;m_c_c[1]<2;m_c_c[1]++){
							m_c_p[1]=mbs_cbsd_TX_power[m_c_c[1]];
							for(m_c_c[2]=0;m_c_c[2]<2;m_c_c[2]++){
								m_c_p[2]=mbs_cbsd_TX_power[m_c_c[2]];
								for(m_c_c[3]=0;m_c_c[3]<2;m_c_c[3]++){
									m_c_p[3]=mbs_cbsd_TX_power[m_c_c[3]];
									for(m_c_c[4]=0;m_c_c[4]<2;m_c_c[4]++){
										m_c_p[4]=mbs_cbsd_TX_power[m_c_c[4]];
										for(m_c_c[5]=0;m_c_c[5]<2;m_c_c[5]++){
											m_c_p[5]=mbs_cbsd_TX_power[m_c_c[5]];
											for(m_c_c[6]=0;m_c_c[6]<2;m_c_c[6]++){
												m_c_p[6]=mbs_cbsd_TX_power[m_c_c[6]];
												for(m_c_c[7]=0;m_c_c[7]<2;m_c_c[7]++){
													m_c_p[7]=mbs_cbsd_TX_power[m_c_c[7]];
													for(m_c_c[8]=0;m_c_c[8]<2;m_c_c[8]++){
														m_c_p[8]=mbs_cbsd_TX_power[m_c_c[8]];
														for(m_c_c[9]=0;m_c_c[9]<2;m_c_c[9]++){
															m_c_p[9]=mbs_cbsd_TX_power[m_c_c[9]];
															for(m_c_c[10]=0;m_c_c[10]<2;m_c_c[10]++){
																m_c_p[10]=mbs_cbsd_TX_power[m_c_c[10]];
																for(m_c_c[11]=0;m_c_c[11]<2;m_c_c[11]++){
																	m_c_p[11]=mbs_cbsd_TX_power[m_c_c[11]];
																	for(m_c_c[12]=0;m_c_c[12]<2;m_c_c[12]++){
																		m_c_p[12]=mbs_cbsd_TX_power[m_c_c[12]];
																		for(m_c_c[13]=0;m_c_c[13]<2;m_c_c[13]++){
																			m_c_p[13]=mbs_cbsd_TX_power[m_c_c[13]];
																			for(m_c_c[14]=0;m_c_c[14]<2;m_c_c[14]++){
																				m_c_p[14]=mbs_cbsd_TX_power[m_c_c[14]];
																				for(m_c_c[15]=0;m_c_c[15]<2;m_c_c[15]++){
																					m_c_p[15]=mbs_cbsd_TX_power[m_c_c[15]];
																					for(m_c_c[16]=0;m_c_c[16]<2;m_c_c[16]++){
																						m_c_p[16]=mbs_cbsd_TX_power[m_c_c[16]];
																						for(m_c_c[17]=0;m_c_c[17]<2;m_c_c[17]++){
																							m_c_p[17]=mbs_cbsd_TX_power[m_c_c[17]];
																							for(m_c_c[18]=0;m_c_c[18]<2;m_c_c[18]++){
																								m_c_p[18]=mbs_cbsd_TX_power[m_c_c[18]];
																								for(m_c_c[19]=0;m_c_c[19]<2;m_c_c[19]++){
																									m_c_p[19]=mbs_cbsd_TX_power[m_c_c[19]];
																									for(m_c_c[20]=0;m_c_c[20]<2;m_c_c[20]++){
																										m_c_p[20]=mbs_cbsd_TX_power[m_c_c[20]];
																										for(m_c_c[21]=0;m_c_c[21]<2;m_c_c[21]++){
																											m_c_p[21]=mbs_cbsd_TX_power[m_c_c[21]];
																											for(m_c_c[22]=0;m_c_c[22]<2;m_c_c[22]++){
																												m_c_p[22]=mbs_cbsd_TX_power[m_c_c[22]];
																												for(m_c_c[23]=0;m_c_c[23]<2;m_c_c[23]++){
																													m_c_p[23]=mbs_cbsd_TX_power[m_c_c[23]];
																													for(m_c_c[24]=0;m_c_c[24]<2;m_c_c[24]++){
																														m_c_p[24]=mbs_cbsd_TX_power[m_c_c[24]];
																														for(m_c_c[25]=0;m_c_c[25]<2;m_c_c[25]++){
																															m_c_p[25]=mbs_cbsd_TX_power[m_c_c[25]];	
																															for(m_c_c[26]=0;m_c_c[26]<2;m_c_c[26]++){
																																m_c_p[26]=mbs_cbsd_TX_power[m_c_c[26]];	
																																for(m_c_c[27]=0;m_c_c[27]<2;m_c_c[27]++){
																																	m_c_p[27]=mbs_cbsd_TX_power[m_c_c[27]];
																																	for(m_c_c[28]=0;m_c_c[28]<2;m_c_c[28]++){
																																		m_c_p[28]=mbs_cbsd_TX_power[m_c_c[28]];
																																		for(m_c_c[29]=0;m_c_c[29]<2;m_c_c[29]++){
																																			m_c_p[29]=mbs_cbsd_TX_power[m_c_c[29]];

																																			sum_p=0.0;
																																			sum_h=0.0;
																																			for(i=0;i<4;i++) sum_p+=m_a_p[i];
																																			sum_h=sum_p;
																																			for(i=0;i<30;i++) sum_p+=m_c_p[i];
																																			if((sum_p)<=75) {
																																				good_c+=1;
																																				if(sum_h>=35) high_c+=1;
																																				printf("%d th good times\n",good_c);
																																			}
																																			
																																		}
																																	}
																																}
																															}
																														}
																													}
																												}
																											}
																										}
																									}
																								}
																							}
																						}
																					}
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}
	printf("good times : %d\n",good_c);
	printf("good and high times : %d\n\n",high_c);
	fprintf(fpw,"good times,%d\n",good_c);
	fprintf(fpw,"good and high times,%d,",high_c);

	printf("hello world\n");
	fclose(fpw);
	return 0;
}
