import numpy as np
import csv

cbsd_num = 31 # cbsd numbers
good_allocation = np.zeros((cbsd_num,2))

file_num = 1 # number of files
open_file_name = ['good_allocation']

# open csv file
for i in range (0,file_num):
    for j in range (5,cbsd_num):
        with open(str(j)+'/'+open_file_name[i]+'.csv') as csvfile:
            rows=csv.reader(csvfile)
            data = []

            # store data
            for row in rows:
                data.append(row)

            if(open_file_name[i]=="good_allocation"):
                good_allocation[j][0] = data[0][1]
                good_allocation[j][1] = data[1][1]
                

for j in range (5,cbsd_num):
    print(good_allocation[j][0])

with open('check.csv', 'w', newline='') as csvfile:
    writer = csv.writer(csvfile)
    writer.writerow(['time'])
    writer.writerow(['CBSD number','good allocation','good and high allocation'])
    for j in range (5,cbsd_num):
        writer.writerow([j,good_allocation[j][0],good_allocation[j][1]])