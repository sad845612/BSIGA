#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <string.h>
#include <pthread.h>

int main(){
	float mbs_TX_power[5]={5,6.25,7.5,8.75,10}; // unit : w
	float mbs_cbsd_TX_power[2]={7.5,0}; // unit : w
	float m_a_p[4],m_c_p[30]; // mbs area 1~4 and mbs to cbsd power
	int m_c[4],m_c_c[30]; // for count
	float sum_p=0.0,sum_h=0.0;
	int all_c=0,good_c=0,high_c=0,better_c=0;
	int i;

	FILE *fpw=NULL;
	fpw=fopen("good_allocation.csv","w");

	for(m_c[0]=0;m_c[0]<5;m_c[0]++){
		m_a_p[0]=mbs_TX_power[m_c[0]];
		for(m_c[1]=0;m_c[1]<5;m_c[1]++){
			m_a_p[1]=mbs_TX_power[m_c[1]];
			for(m_c[2]=0;m_c[2]<5;m_c[2]++){
				m_a_p[2]=mbs_TX_power[m_c[2]];
				for(m_c[3]=0;m_c[3]<5;m_c[3]++){
					m_a_p[3]=mbs_TX_power[m_c[3]];
					for(m_c_c[0]=0;m_c_c[0]<2;m_c_c[0]++){
						m_c_p[0]=mbs_cbsd_TX_power[m_c_c[0]];
						for(m_c_c[1]=0;m_c_c[1]<2;m_c_c[1]++){
							m_c_p[1]=mbs_cbsd_TX_power[m_c_c[1]];
							for(m_c_c[2]=0;m_c_c[2]<2;m_c_c[2]++){
								m_c_p[2]=mbs_cbsd_TX_power[m_c_c[2]];
								for(m_c_c[3]=0;m_c_c[3]<2;m_c_c[3]++){
									m_c_p[3]=mbs_cbsd_TX_power[m_c_c[3]];
									for(m_c_c[4]=0;m_c_c[4]<2;m_c_c[4]++){
										m_c_p[4]=mbs_cbsd_TX_power[m_c_c[4]];
										for(m_c_c[5]=0;m_c_c[5]<2;m_c_c[5]++){
											m_c_p[5]=mbs_cbsd_TX_power[m_c_c[5]];
											for(m_c_c[6]=0;m_c_c[6]<2;m_c_c[6]++){
												m_c_p[6]=mbs_cbsd_TX_power[m_c_c[6]];
												for(m_c_c[7]=0;m_c_c[7]<2;m_c_c[7]++){
													m_c_p[7]=mbs_cbsd_TX_power[m_c_c[7]];

													sum_p=0.0;
													sum_h=0.0;
													for(i=0;i<4;i++) sum_p+=m_a_p[i];
													sum_h=sum_p;
													for(i=0;i<8;i++) sum_p+=m_c_p[i];
													if((sum_p)<=75) {
														good_c+=1;
														if(sum_h>=35) high_c+=1;
														printf("%d th good times\n",good_c);
													}
													
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}
	printf("good times : %d\n",good_c);
	printf("good and high times : %d\n\n",high_c);
	fprintf(fpw,"good times,%d\n",good_c);
	fprintf(fpw,"good and high times,%d,",high_c);

	printf("hello world\n");
	fclose(fpw);
	return 0;
}
