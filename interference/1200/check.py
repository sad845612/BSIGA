import numpy as np
import csv

ue_num = 1200 # ue numbers
cbsd_num = 11 # cbsd numbers

ES_t = np.zeros(cbsd_num) # throughput
ES_p = np.zeros(cbsd_num) # power
ES_r = np.zeros(cbsd_num) # relay people

ES_same_t = np.zeros(cbsd_num) # throughput
ES_same_p = np.zeros(cbsd_num) # power
ES_same_r = np.zeros(cbsd_num) # relay people

file_num = 2 # number of files
open_file_name = ['Exhaustive','Exhaustive_same']

# open csv file
for i in range (0,file_num):
    for j in range (5,cbsd_num):
        with open(str(j)+'/'+open_file_name[i]+'.csv') as csvfile:
            rows=csv.reader(csvfile)
            data = []

            # store data
            for row in rows:
                data.append(row)

            if(open_file_name[i]=="Exhaustive"):
                ES_t[j] = float(data[6][0])
                for k in range (0,j):
                    ES_p[j] = ES_p[j] + float(data[4][4+k])
                for l in range (0,ue_num):
                    if(data[9+l][5]!='0'):
                        ES_r[j] = ES_r[j] + 1
            elif(open_file_name[i]=="Exhaustive_same"):
                ES_same_t[j] = float(data[6][0])
                for k in range (0,j):
                    ES_same_p[j] = ES_same_p[j] + float(data[4][4+k])
                for l in range (0,ue_num):
                    if(data[9+l][5]!='0'):
                        ES_same_r[j] = ES_same_r[j] + 1

for j in range (5,cbsd_num):
    print(ES_t[j])

with open('check.csv', 'w', newline='') as csvfile:
    writer = csv.writer(csvfile)
    writer.writerow(['Algorithm result'])
    writer.writerow(['CBSD number','ES','','','ES same CBSD power','',''])
    for j in range (5,cbsd_num):
        writer.writerow([j,ES_t[j],ES_p[j],ES_r[j],ES_same_t[j],ES_same_p[j],ES_same_r[j]])
    